package com.huy.phonemanager.model;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class UserDataProvider {
    private static List<User> users = new ArrayList<>();

    static {
        users.add(new User("admin", "admin"));
        users.add(new User("tranmani", "tranmani"));
    }

    public static List<User> getUsers() {
        return users;
    }

    public static void addUser(String username, String password) {
        users.add(new User(username, password));
    }

    /**
     * Validate username and password when user using login form
     * @param username
     * @param password
     * @return
     */
    public static boolean validateUsernamePassword(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get 1 user by their username in the database
     * @param username username to get
     * @return
     */
    public static User getUserByUsername(String username) {
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(username)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Check if the given username is already in use in the database
     * @param username username to check
     * @return
     */
    public static boolean isDuplicate(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for user to update their profile
     * @param username username of the user
     * @param name new name to change
     * @param age new age to change
     */
    public static void updateProfile (String username, String name, int age) {
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(username)) {
                user.setName(name);
                user.setAge(age);
            }
        }
    }

    /**
     * Method to check if user is logged in
     * @param session
     * @return
     */
    public static boolean isLoggedIn(HttpSession session) {
        return session.getAttribute("username") != null;
    }

    /**
     * Get cookie value by using cookie name
     * @param request
     * @param cookieName
     * @return
     */
    public static String extractCookieByName(HttpServletRequest request, String cookieName) {
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals(cookieName))
                return cookie.getValue();
        }
        return null;
    }

}


