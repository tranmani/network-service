package com.huy.phonemanager.model;

public class Phone {
    private String phoneName, description, imgsrc;
    private double price;
    private static int lastId = 1;
    private int id, view = 0;

    public Phone(String phoneName, double price, String description, String imgsrc) {
        this.id = lastId++;
        this.phoneName = phoneName;
        this.price = price;
        this.description = description;
        this.imgsrc = imgsrc;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public int getId() {
        return id;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgsrc() {
        return imgsrc;
    }

    public int getView() {
        return view;
    }

    public void increaseView() {
        this.view += 1;
    }
}
