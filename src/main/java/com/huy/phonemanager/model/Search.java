package com.huy.phonemanager.model;

public class Search {
    private String searchQuery;

    public Search(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    @Override
    public String toString() {
        return searchQuery;
    }
}
