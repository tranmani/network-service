package com.huy.phonemanager.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class User {
    private String username, password, name, createdOn;
    private int age;

    public User(String username, String password) {
        LocalDate today = LocalDate.now();
        String formattedDate = today.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"));

        this.username = username;
        this.password = password;
        this.name = "Not set yet";
        this.age = 0;
        this.createdOn = formattedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
