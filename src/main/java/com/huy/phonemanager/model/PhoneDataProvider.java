package com.huy.phonemanager.model;

import java.util.ArrayList;
import java.util.List;

public class PhoneDataProvider {
    private static List<Phone> phones = new ArrayList<>();
    private static List<Phone> phoneListForSearchPage;

    static {
        phones.add(new Phone("LG Q7", 349, "Cheap", "phone-img/lgq7.jpg"));
        phones.add(new Phone("Nokia 9", 599, "Old dude trying to be young again", "phone-img/nokia9.jpg"));
        phones.add(new Phone("Samsung S10 Plus", 999, "Infinity O", "phone-img/s10plus.jpg"));
        phones.add(new Phone("Xiaomi Mi 9", 470, "Flagship killer", "phone-img/mi9.jpg"));
        phones.add(new Phone("Apple iPhone 8", 700, "Over priced", "phone-img/iphone8.jpg"));
        phones.add(new Phone("Oneplus 7 Pro", 759, "Flagship killer 2.0", "phone-img/7pro.png"));
        phones.add(new Phone("Apple iPhone X", 835, "Notch", "phone-img/iphonex.jpg"));
        phones.add(new Phone("Apple iPhone XS Max", 1279, "Notch 2.0", "phone-img/iphonexsmax.jpg"));
        phones.add(new Phone("Asus Zenfone 6", 499, "Flagship killer 3.0", "phone-img/zenfone6.jpg"));
        phones.add(new Phone("Redmi K20 Pro", 377, "Flagship killer 4.0", "phone-img/k20pro.jpg"));
    }

    public static List<Phone> getPhones() {
        return phones;
    }

    public static List<Phone> getPhoneListForSearchPage() {
        return phoneListForSearchPage;
    }


    /**
     * Loop through the phone database and copy matched phone name to different list and return it
     * @param search search query
     * @return
     */
    public static List<Phone> transferToSearch(Search search) {
        phoneListForSearchPage = new ArrayList<>();
        for (Phone phone : phones) {
            if (phone.getPhoneName().toLowerCase().contains(search.toString().toLowerCase())) {
                phoneListForSearchPage.add(phone);
            }
        }
        return phoneListForSearchPage;
    }

    /**
     * Get 1 phone by id in the database
     * @param id
     * @return
     */
    public static Phone getPhoneById(int id) {
        for (Phone phone : phones) {
            if (phone.getId() == id) {
                return phone;
            }
        }
        return null;
    }

    public static void updatePhone(int id, String phoneName, double price, String description) {
        Phone phone = getPhoneById(id);

        phone.setPhoneName(phoneName);
        phone.setPrice(price);
        phone.setDescription(description);
    }

    public static void deletePhone(int id) {
        Phone phone = getPhoneById(id);

        phones.remove(phone);
    }

    public static void addPhone (String phoneName, double price, String description) {
        phones.add(new Phone(phoneName, price, description, "phone-img/default-phone.jpg"));
    }
}
