package com.huy.phonemanager.controller;

import com.huy.phonemanager.model.UserDataProvider;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class ProfileController {
    /**
     * Display profile page
     * @param model
     * @param session
     * @param request
     * @return
     */
    @GetMapping(path = "profile")
    public String profilePage(Model model, HttpSession session, HttpServletRequest request) {
        if (!UserDataProvider.isLoggedIn(session)) {
            // If user is not logged in, redirect user to home page
            return "redirect:/";
        } else {
            //Get username of the logged in user
            String loggedInUsername = (session.getAttribute("username").toString());

            model.addAttribute("user", UserDataProvider.getUserByUsername(loggedInUsername));
            model.addAttribute("lastVisitedDate", UserDataProvider.extractCookieByName(request, "lastVisitedDate"));
        }
        return "profile";
    }

    /**
     * Handle edit profile process
     * @param name
     * @param age
     * @param session
     * @return
     */
    @PostMapping(path = "profile")
    public String editProfile(@RequestParam String name, @RequestParam int age, HttpSession session) {
        //Get username of the logged in user
        String usernameLogged = (session.getAttribute("username").toString());
        UserDataProvider.updateProfile(usernameLogged, name, age);
        return "redirect:/profile";
    }
}
