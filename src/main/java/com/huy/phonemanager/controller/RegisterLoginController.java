package com.huy.phonemanager.controller;

import com.huy.phonemanager.model.User;
import com.huy.phonemanager.model.UserDataProvider;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller
@RequestMapping("/")
public class RegisterLoginController {
    /**
     * Display register page
     * @param model
     * @param user
     * @param session
     * @return
     */
    @GetMapping(path = "register")
    public String registerPage(Model model, User user, HttpSession session) {
        model.addAttribute("user", user);
        model.addAttribute("users", UserDataProvider.getUsers());
        if (UserDataProvider.isLoggedIn(session)) {
            // If user is logged in, redirect user to home page
            return "redirect:/";
        }
        return "register";
    }

    /**
     * Handle register process
     * @param username
     * @param password
     * @param model
     * @return
     */
    @PostMapping(path = "/register")
    public String addUser(@RequestParam(value = "username") String username, @RequestParam(value = "password") String password, Model model) {
        //Check if the username is already in use
        if (UserDataProvider.isDuplicate(username)) {
            model.addAttribute("duplicateUsername", "Username has been taken, please use different username!");
            return "register";
        } else {
            //Else add to database
            UserDataProvider.addUser(username, password);
            return "redirect:/login";
        }
    }

    /**
     * Display login page
     * @param session
     * @return
     */
    @GetMapping(path = "login")
    public String loginPage(HttpSession session) {
        if (UserDataProvider.isLoggedIn(session)) {
            // If user is logged in, redirect user to home page
            return "redirect:/";
        }
        return "login";
    }

    /**
     * Handle login process
     * @param username
     * @param password
     * @param model
     * @param session
     * @param response
     * @return
     */
    @PostMapping(path = "login")
    public String login(@RequestParam(value = "username") String username, @RequestParam(value = "password") String password,
                        Model model, HttpSession session, HttpServletResponse response) {
        if (UserDataProvider.validateUsernamePassword(username, password)) {
            //Get logged in username
            session.setAttribute("username", username);

            //Set cookie when successful login
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
            Calendar cal = Calendar.getInstance();
            String date = dateFormat.format(cal.getTime());

            //Encode date
            try {
                String encodedCookieValue = URLEncoder.encode(date, "UTF-8");
                Cookie cookie = new Cookie("lastVisitedDate", encodedCookieValue);
                response.addCookie(cookie);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return "redirect:/";
        } else {
            model.addAttribute("errormessage", "Username or Password is not valid!");
            return "login";
        }
    }

    /**
     * Handle logout process
     * @param session
     * @return
     */
    @GetMapping(path = "/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }
}

