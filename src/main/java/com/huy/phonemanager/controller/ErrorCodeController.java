package com.huy.phonemanager.controller;

import com.huy.phonemanager.model.UserDataProvider;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ErrorCodeController implements ErrorController {
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model, HttpSession session) {
        boolean isloggedIn = false;
        if (UserDataProvider.isLoggedIn(session)) {
            isloggedIn = true;
        }
        model.addAttribute("isLoggedIn", isloggedIn);

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error/error-404";
            } else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "error/error-500";
            } else if(statusCode == HttpStatus.BAD_REQUEST.value()) {
                return "error/error-400";
            }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}