package com.huy.phonemanager.controller;

import com.huy.phonemanager.model.PhoneDataProvider;
import com.huy.phonemanager.model.Search;
import com.huy.phonemanager.model.UserDataProvider;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class PhoneController {
    private List<String> searchedQueries = new ArrayList<>();

    /**
     * Display all phones
     * @param model
     * @param search
     * @param session
     * @return
     */
    @GetMapping(path = "")
    public String index(Model model, Search search, HttpSession session) {
        model.addAttribute("phones", PhoneDataProvider.getPhones());
        model.addAttribute("search", search);
        boolean isloggedIn = false;
        if (UserDataProvider.isLoggedIn(session)) {
            isloggedIn = true;
        }
        model.addAttribute("isLoggedIn", isloggedIn);
        return "index";
    }

    /**
     * Handle search function
     * @param searchQuery searchQuery
     * @param model
     * @param search
     * @return
     */
    @GetMapping(path = "search")
    public String searchFunction(@RequestParam(value = "searchQuery", required=false) String searchQuery,
                                 Model model, Search search, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        model.addAttribute("search", search);
        model.addAttribute("phones", PhoneDataProvider.transferToSearch(search));

        Cookie cookie = new Cookie("searched", searchQuery);
        cookie.setMaxAge(60*60);
        response.addCookie(cookie);

        searchedQueries.add(UserDataProvider.extractCookieByName(request, "searched"));
        model.addAttribute("searchedQueries", searchedQueries);
        //If cookie is expire, empty the list
        if (UserDataProvider.extractCookieByName(request, "searched")==null) {
            searchedQueries.clear();
        }

        boolean isloggedIn = false;
        if (UserDataProvider.isLoggedIn(session)) {
            isloggedIn = true;
        }
        model.addAttribute("isLoggedIn", isloggedIn);
        return "search";
    }

    /**
     * Display phone detail page
     * @param id
     * @param model
     * @param session
     * @return
     */
    @GetMapping(path = "phone/{id}")
    public String phoneDetail(@PathVariable(value = "id") int id, Model model, HttpSession session) {
        model.addAttribute("phone", PhoneDataProvider.getPhoneById(id));
        model.addAttribute("isLoggedIn", !UserDataProvider.isLoggedIn(session));

        //View counter, increase 1 each times someone view phone detail
        PhoneDataProvider.getPhoneById(id).increaseView();

        return "phone";
    }

    /**
     * Delete phone function
     * @param id
     * @param session
     * @return
     */
    @GetMapping(path = "phone/{id}/delete")
    public String deletePhone(@PathVariable(value = "id", required = false) int id, HttpSession session) {
        if (!UserDataProvider.isLoggedIn(session)) {
            return "redirect:/";
        } else {
            PhoneDataProvider.deletePhone(id);
            return "redirect:/";
        }
    }

    /**
     * Handle edit phone process
     * @param id
     * @param phoneName
     * @param price
     * @param description
     * @return
     */
    @PostMapping(path = "phone/{id}")
    public String editPhone(@PathVariable(value = "id") int id, @RequestParam String phoneName, @RequestParam double price, @RequestParam String description) {
        PhoneDataProvider.updatePhone(id, phoneName, price, description);
        return "redirect:/phone/{id}";
    }

    /**
     * Display add phone page
     * @param model
     * @param session
     * @return
     */
    @GetMapping(path = "add-phone")
    public String displayAddPhonePage(Model model, HttpSession session) {
        boolean isloggedIn = false;
        if (UserDataProvider.isLoggedIn(session)) {
            isloggedIn = true;
        }
        model.addAttribute("isLoggedIn", isloggedIn);
        return "add-phone";
    }

    /**
     * Handle add phone process
     * @param phoneName
     * @param price
     * @param description
     * @return
     */
    @PostMapping(path = "add-phone")
    public String addPhone(@RequestParam String phoneName, @RequestParam double price, @RequestParam String description) {
        PhoneDataProvider.addPhone(phoneName, price, description);
        return "redirect:/";
    }
}